const { once } = require('events');
const fs = require('fs');
const path = require('path');
const sharp = require('sharp');
const stream = require('stream');
const util = require('util');

const finished = util.promisify(stream.finished);

const KNOWN_EXTENSIONS = ['.jpg', '.jpeg', '.png', '.tiff', '.gif', '.svg', '.webp'];
const ERROR_FILE = 'corrupted.txt';

function hasKnownExtension(file) {
  return KNOWN_EXTENSIONS.includes(path.extname(file).toLowerCase());
}

async function main() {
  let args = process.argv.slice(2);
  let root = args[0];

  var log = null;
  var files = fs.readdirSync(root).filter(hasKnownExtension);
  for (var file of files)
  {
    try {
      await sharp(path.join(root, file), { failOnError: false }).stats();
    } catch (e) {
      console.log(file, e);

      if (log == null) {
        log = fs.createWriteStream(ERROR_FILE);
      }
      if (!log.write(`${file}\n`)) {
        await once(log, 'drain');
      }
    }
  }

  if (log != null) {
    log.end();
    await finished(log);
    console.log(`Corrupt files logged to ${ERROR_FILE}`);
  }
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
